from operator import mod
from pyexpat import model
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from todos.models import TodoList
from django.views.generic.edit import CreateView

# Create your views here.
class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"


class TodoDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"


class TodoCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    fields = ["name"]
